# kunesti-docs

Kunesti is an event scheduling platform which supports event planning discussions and polling event guests.

## Use Cases

Common use cases are documented [here](use-cases.md)