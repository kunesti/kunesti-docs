# REST API

## Routing 

Any request to the server for paths starting with /groups /events /account will have the index.html file served back to facilitate client side routing.

## Auth Endpoint

Specific parameter names and exact details need checked against the spec. This draft is mostly from memory.

Summary: Kunesti servers must implement OAuth2 Authorization Flow with PKCE Extension.  Kunesti servers SHOULD implement OAuth 2.0 Authorization Server Metadata ( OAuth Discovery ) ( https://datatracker.ietf.org/doc/html/rfc8414 ).  The specific mechanism used to authenticate the user is left to the server. ( "Social" login, email code, rsa key etc. )

### OAuth 2.0 Authorization Flow with Proof Key for Code Exchange (PKCE) Extension sequence

```mermaid
sequenceDiagram
    Client ->> Authorization Server: Redirect user to authorization server requesting authorization token
    Note over Client, Authorization Server: client_id (client url for indieauth), code_challenge, <br/>code_challenge_method, redirect_uri, state
    Authorization Server ->> Authorization Server: Present login options if needed, then present request for authorization.
    Authorization Server ->> Client: Redirect back to client with authorization_code and repeate state parameter (and 'me' for IndieAuth)
    Note over Client: In the case of IndieAuth the user can be considered authenticated for the client at this point
    Client ->> Authorization Server: Request access_token using authorization_code, client_secret (only oauth2.0), and code_verifier (PKCE extension)
    Authorization Server ->> Client: Return access_token and refresh_token, with lifespan of token in seconds
    Client ->> Resource Server: Request data using access_token
```

### GET /oauth/authorize

Request Query Parameters:
- client_id
- code_challenge
- code_challenge_method
- redirect_uri
- scope
- state

Clients should redirect users to the oauth authorization path for a specific Kunesti server.  If the Kunesti server implements OAuth Discovery the client can accept a username ( in username@domain.tld format ) and perform the necessary steps to determine the correct server.  Otherwise determining the server is likely configured by the server ( host of the client ) operator.

Response: A redirect back to the redirect_uri with the authorization_code, or no redirect back at all if the user rejects the authorization request or could not be authenticated.

### GET /oauth/token

Request Query Parameters:
- code_verifier
- authorization_code

Response: access_token, refresh_token, expires_in

## Account Endpoint

TODO

## Notification Endpoint

### POST /api/notifications

Description: Query for notifications.

Request Headers: 
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [NotificationQuery](./api-types.md#Notification-Types) object.

Request Body: [NotificationQueryResponse](./api-types.md#Notification-Types) object.

## Invitation Endpoint

### GET /api/invites

Description: Get a collection of pending invitations for current user.

Request Headers: 
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [InvitationCollection](./api-types.md#Invitation-Types) object.

## Group Endpoint

### GET /api/groups

Description: Get a list of groups that the user is a member of.

Request Headers: 
- Authorization: Bearer \$SESSION_TOKEN

Response Body: Non null list of [Group](./api-types.md#Group-Types) objects.

### POST /api/groups

Description: Create a new group.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [Group](./api-types.md#Group-Types) object without id (will be ignored if provided). Invites will be sent to users in member list.

Response Code: 201 Created upon success

Response Headers:
- Location: /api/groups/\$GROUP_ID

Response Body: [Group](./api-types.md#Group-Types) object with id.

### PUT /api/groups/$GROUP_ID

Description: Update a group.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [Group](./api-types.md#Group-Types) object. Group name and members list will be updated. Existing members not in object will be removed from the group. Invites will be sent to new members.

Response Code: 200 upon success, 403 if user does not have modify permissions on group.

Response Body: None.

### DELETE /api/groups/\$GROUP_ID

Description: Delete a group.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success, 403 if user does not have modify permissions on group.

### GET /api/invites/group

Description: Get pending group invites for current user.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: list of [JoinGroupInvitation](./api-types.md#Group-Types) objects.

### GET /api/invites/group/\$INVITE_ID

Description: Get pending group invite for current user.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [JoinGroupInvitation](./api-types.md#Group-Types) object.

### GET /api/groups/\$GROUP_ID/response

Description: respond to an invitation to join a group from an external link. Browser is redirected to client routed URL to display result which is provided in Location URL parameters. Possible \$FAILURE_CODE values:
- 100 Unknown Group
- 101 Invalid Invite Token

URL Query Parameters:
- inviteToken=\$INVITE_TOKEN
- answer=```[accept|decline]```

Response Code: 303

Response Headers:
- Location: /groups/response?groupId=$GROUP_ID&result=```[accepted|declined|failure]```&code=\$FAILURE_CODE

### POST /api/groups/\$GROUP_ID/response

Description: respond to an invitation to join a group without redirect. session token is used if body does not contain invite token.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [GroupInvitationResponse](./api-types.md#Group-Types) object.

Response Code: 200 on success, 400 if no invitation for user is found.

### GET /api/groups/\$GROUP_ID/users

Description: get a list of users for group.

Request Body: list of [GroupUser](./api-types.md#Group-Types) objects.

### DELETE /api/groups/\$GROUP_ID/users/\$USERNAME

Description: remove a specific user from group. Users always have authorization to remove themselves from group, only admins can remove others.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 on success, 403 when not allowed

### GET /api/groups/\$GROUP_ID/unsubscribe

Description: leave group using external link. Membership token is user-specific server-generated token which can be sent via email for unsubscribe links. Browser is redirect to client routed URL to display result which is provided in Location URL parameters. Possible $FAILURE_CODE values:
- 100 Unknown Group
- 101 Invalid Membership Token


URL Query Parameters:
- membershipToken=\$MEMBERSHIP_TOKEN

Response Code: 303

Response Headers:
- Location: /groups/unsubscribe?groupId=\$GROUP_ID&result=```[success|failure]```&code=\$FAILURE_CODE

## Event Query Endpoint

### POST /api/event-query

Description: Search for events that the user has access to.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventQuery](./api-types.md#Event-Query-Types) object. If no query fields are provided (empty object as body), the server will return details for all events for user (server may limit responses).

Response Body: Non null list of [EventSummary](./api-types.md#Event-Query-Types) objects.

## Event Endpoint

### POST /api/events

Description create new event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventCreationRequest](./api-types.md#Event-Types) object

Response Code: 201 on success

Response Headers: Location /api/events/\$EVENT_ID

Response Body: [Event](./api-types.md#Event-Types) object

### GET /api/events/\$EVENT_ID

Description: Get full information for event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [Event](./api-types.md#Event-Types) object

### DELETE /api/events/\$EVENT_ID

Description: Delete an event and all associated data. Shouldn't be used regularly. In normal situations, the EventDetails should be updated to be cancelled instead.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success, 403 if user does not have modify permissions on event.

### GET /api/invites/event

Description: Get pending event invites for current user.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: list of [EventInvitation](./api-types.md#Group-Types) objects.

### GET /api/invites/event/\$INVITE_ID

Description: Get pending event invite for current user.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [EventInvitation](./api-types.md#Group-Types) object.

### GET /api/events/\$EVENT_ID/rsvp

Description: respond to an invitation to an event from an external link. Invite token is user-specific server-generated token which can be sent via email. Browser is redirect to client routed URL to display result which is provided in Location URL parameters. Possible \$FAILURE_CODE values:
- 100 Unknown Group
- 101 Invalid Invite Token

URL Query Parameters:
- inviteToken=\$INVITE_TOKEN
- answer=```[decline|unlikely|maybe|likely|accept]```
- applyToSeries=```[true|false]```

Response Code: 303

Response Headers:
- Location: /events/response?eventId=\$EVENT_ID&result=```[decline|unlikely|maybe|likely|accept|failure]```&code=\$FAILURE_CODE


### POST /api/events/\$EVENT_ID/rsvp

Description: respond to an invitation to event without redirect. session token is used if body does not contain invite token.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventInvitationResponse](./api-types.md#Event-Types) object.

Response Code: 200 on success, 400 if no invitation for user is found.

### PUT /api/events/\$EVENT_ID/details

Description: Update event details.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventDetails](./api-types.md#Event-Types) object

Response Code: 200 upon success, 403 if user does not have modify permissions on event.

### GET /api/events/\$EVENT_ID/details

Description: Get event details.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [EventDetails](./api-types.md#Event-Types) object

### POST /api/events/\$EVENT_ID/guests

Description: Update event guest list. Existing groups/users that aren't in the request object will be removed from the event. New groups/users will be invited to the event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [InviteList](./api-types.md#Event-Types) object

Response Code: 200 upon success, 403 if user does not have modify permissions on event.

Response Body: [GuestList](./api-types.md#Event-Types) object

### GET /api/events/\$EVENT_ID/guests

Description: Get guest list for event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: [GuestList](./api-types.md#Event-Types) object

### POST /api/events/\$EVENT_ID/recurrence

Description: Update recurrence schedule for event. Future occurrences will be rescheduled as a result.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [Recurrence](./api-types.md#Event-Types) object

Response Code: 200 upon success, 403 if user does not have modify permissions on event.

### GET /api/events/\$EVENT_ID/recurrence

Description: Get recurrence schedule for event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: [Recurrence](./api-types.md#Event-Types) object

### GET /api/events/\$EVENT_ID/next

Description: Get next occurrence of event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

URL Query Parameters:
- after= optional ISO 8601 time stamp, server may limit how man future occurrence can be accessed

Response Code: 200 upon success, 404 if server does not have occurrence after provided date time

Response Body: [EventOccurrence](./api-types.md#Event-Types) object

### GET /api/events/\$EVENT_ID/history

Description: Get historical occurrences of event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

URL Query Parameters:
- limit=optional limit of response, response will be sorted with most recent occurrences first

Response Code: 200 upon success

Response Body: non-nullable list of [EventSummary](./api-types.md#Event-Types) objects for past occurrences available on server (API does not dictate how long a server must store old event occurrences)

### GET /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID

Description: Get specific occurrence event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: non-nullable [EventOccurrence](./api-types.md#Event-Types) object

### POST /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/rsvp

Description: respond to a specific occurrence of event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventInvitationResponse](./api-types.md#Event-Types) object.

Response Code: 200 on success, 400 if no invitation for user is found.

### PUT /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/details

Description: Update event details for a specific occurrence.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [EventDetails](./api-types.md#Event-Types) object

Response Code: 200 upon success, 403 if user does not have modify permissions on event.

### GET /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/details

Description: Get event details for a specific occurrence.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Body: [EventDetails](./api-types.md#Event-Types) object

### GET /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/guests

Description: Get guest list for specific occurrence of event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: [GuestList](./api-types.md#Event-Types) object

### POST /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments

Description: Post a new comment to an event occurrence.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [Comment](./api-types.md#Event-Types) object

Response Code: 201 upon success, 403 if user does not have comment permissions on event.

Response Headers:
- Location: /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments/\$COMMENT_ID

Response Body: [Comment](./api-types.md#Event-Types) object with comment ID populated.

### GET /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments

Description: Get comments for specific occurrence of event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: Non null list of [Comment](./api-types.md#Event-Types) objects.

### GET /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments/\$COMMENT_ID

Description: Get specific comment for event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success

Response Body: [Comment](./api-types.md#Event-Types) object.

### PUT /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments/\$COMMENT_ID

Description: Update a comment on an event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Request Body: [Comment](./api-types.md#Event-Types) object

Response Code: 200 upon success, 403 if user does not have edit permissions on comment.

### DELETE /api/events/\$EVENT_ID/occurrence/\$OCCURRENCE_ID/comments/\$COMMENT_ID

Description: Delete a comment on an event.

Request Headers:
- Authorization: Bearer \$SESSION_TOKEN

Response Code: 200 upon success, 403 if user does not have edit permissions on comment.