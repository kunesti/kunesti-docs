# Use Cases

These are some general use cases for the platform and is not an exhaustive list.

## Creating an Event

Preconditions: Users Alex, Bruce, Charlie and Daphne have created accounts with kunesti. Alex, Bruce and Charlie are in a kunesti group called "Movie Club", Daphne is not.

Alex uses a browser to connect to the kunesti web app. Alex clicks the icon that says 'New Event'. The create event dialog is displayed which takes in the event name (required), description (required), location (optional), time (optional) and list of invitees (optional). Alex enters "Star Wars Movie" as the event name, "Let's watch the original 1977 theatrical release of Star Wars" as the description, adds "Movie Club" and Daphne's email address to the invitees list and clicks "create". Bruce, Charlie and Daphne each get an email with the name of the event as the title, the description of the event and a link to the event page on kunesti.

## Posting to an Event Discussion

Preconditions: Alex has created an event called "Star Wars Movie". Bruce has posted a comment on the event discussion.

Daphne navigates to the event page for the Star Wars Movie event. A list of previous comments is listed at the bottom of the page followed by a blank text field. Daphne enters a comment saying "I've already seen it" in the provided field and clicks the "post" button. The new comment now appears at the bottom of the comments.

## Declining an Event

Preconditions: Alex has created an event and invited Bruce, Charlie and Daphne.

Daphne navigates to the event page for the Star Wars Movie event. A list of invitees is displayed on one side of the page with Alex listed as "attending" and Bruce, Charlie and Daphne listed as "not responded". Daphne clicks the "Decline" icon. The list of invitees updates to show Daphne as "declined". Alex gets an email stating that Daphne declined the event. Daphne receives no more email about the event.

## Accepting an Event

Preconditions: Alex has created an event and invited Bruce, Charlie and Daphne. Daphne has declined the event.

Bruce navigates to the event page for the Star Wars Movie event. A list of invitees is displayed on one side of the page with Alex listed as "attending", Bruce and Charlie  listed as "not responded" and Daphne listed as "declined". Bruce clicks the "Accept" icon. The list of invitees updates to show Bruce as "attending". Alex gets an email stating that Bruce accepted the event.

## Adding a Poll to an Event

Preconditions: Alex has created an event called "Star Wars Movie" which has Bruce, Charlie and Daphne as invitees. Daphne has declined the event.

Alex navigates to the "Star Wars Movie" event page and clicks the "Add Poll" icon. The create poll dialog is displayed which takes in the poll title and a list of poll options. Alex enters "Which day works best?" as the title, enters two days for the options and clicks "create". Bruce and Charlie each get an email with the title of the poll and a link to the event page which now contains the poll at the top of the page. Daphne does not get an email. Bruce and Charlie both navigate to the event page, select the first option in the poll and click "submit". The poll is replaced by the poll results. Alex gets two emails with poll results. 

## Adding Time and Location to an Event

Preconditions: Alex has created an event called "Star Wars Movie" which does not have a time or location and has the "Movie Club" group as invited.

Alex navigates to the "Star Wars Movie" event page and clicks the "Edit" icon. The edit event dialog is displayed which takes in the event name (already populated with "Star Wars Movie"), description (already populated), time (unpopulated), location (unpopulated) and a list of invitees (populated with Movie Club). Alex populates the time and location fields and clicks "submit". Each member of the movie club group gets an email which includes an iCalendar file as an attachment and a link to the event page.