# Types used in APIs

## Auth Types

TODO

## Account Types

TODO

## Notification Types
```
type NotificationQuery {
    limit: nullable positive number to limit the number of notifications returned
    bookmarkToken: nullable server-issued token to mark last notification received by client
}
```
```
type GroupInviteNotificationContext{
    type: always a string with value of GROUP_INVITE_NOTICE
    invitationId: non-nullable group invite ID
}
```
```
type EventInviteNotificationContext{
    type: always a string with value of EVENT_INVITE_NOTICE
    invitationId: non-nullable event invite ID
}
```
```
union NotificationContext = GroupInviteNotificationContext | EventInviteNotificationContext
```
```
type NotificationContext{
    type: always a string with value of EVENT_INVITE_NOTICE
    groupId: non-nullable event ID
}
```
```
type Notification {
    id: non-nullable unique id for notification
    timestamp: non-nullable ISO-8601 date time
    text: non-nullable display text for notification
    context: nullable NotificationContext object
}
```
```
type NotificationQueryResponse {
    notifications: non-nullable list of Notification objects
    bookmarkToken: non-nullable server-issued token to mark last notification in response
    moreAvailable: boolean, true and present if more notifications are available on server
}
```

## Invitation Types

```
type JoinGroupInvitation {
    id: unique and immutable id of invitation
    groupId: non-null identifier of group
    groupName: non-null display name of group
    inviteToken: non-null user-specific server-generated token
}
```
```
type EventInvitation {
    id: unique and immutable id of invitation
    eventId: non-null identifier of event
    occurrenceId: nullable identifier of occurrence if this invite is for a specific occurrence
    eventName: non-null display name of event
    inviteToken: non-null user-specific server-generated token
}
```
```
type InvitationCollection {
    groupInvites: non-null list of JoinGroupInvitation objects
    eventInvites: non-null list of EventInvitation objects
}
```
## Group Types

```
type GroupInvitationResponse {
    inviteToken: nullable user-specific server-generated token, session token used if missing
    answer: non-null string enum with following possible values [decline|unlikely|maybe|likely|accept]
}
```
```
type GroupUser {
    username: non-null unique user identifier
    role: string enum [PENDING|MEMBER|ADMIN]
}
```
```
type Group {
    id: nullable unique and immutable ID of group
    name: non-null display name of group
    users: non-null list of GroupUser objects
}
```

## Event Query Types
```
type TimeRange {
    start: non-nullable inclusive ISO-8601 datetime
    end: non-nullable inclusive ISO-8601 datetime
}
```
```
type EventQuery {
    text: nullable query string for text search
    range: nullable TimeRange
    limit: nullable positive integer to limit the size of query response
}
```
``` 
type EventSummary {
    eventId: non-nullable unique and immutable ID of event
    occurrenceId: non-nullable immutable and unique within event
    title: non-nullable display name of event
    startTime: non-nullable ISO-8601 datetime of occurrence
}
```

## Event Types
```
type InviteList {
    groups: non-nullable list of group IDs to invite
    users: non-nullable list of usernames/emails to invite
}
```
```
type EventInvitationResponse {
    inviteToken: nullable user-specific server-generated token, session token used if missing
    answer: non-null string enum [decline|unlikely|maybe|likely|accept]
    applyToSeries: boolean true if answer should automatically be applied to all events in series or if the user should be separately invited to each occurrence of event. false if null
}
```
```
type WeeklySchedule {
    type: always a string with value WEEKLY
    days: non-nullable list of enum [MON|TUE|WED|THU|FRI|SAT|SUN]
}
```
```
type MonthlySchedule {
    type: always a string with value MONTHLY
    positions: non-nullable list of enum [FIRST|SECOND|THIRD|FOURTH|LAST]
    day: non-nullable enum [MON|TUE|WED|THU|FRI|SAT|SUN]
}
```
```
union Schedule = WeeklySchedule | MonthlySchedule
```
```
type Recurrence {
    schedule: non-nullable Schedule object
    until: nullable ISO-8601 datetime
}
```
```
type Cancellation {
    cancelled: non-nullable boolean
    message: nullable message
}
```
```
type EventDetails {
    title: non-null display name of event
    description: non-null description of event
    location: nullable location of event
    startTime: non-nullable ISO-8601 datetime
    duration: nullable positive integer minutes
    cancellation: non-nullable Cancellation object
}
```
```
type EventCreationRequest {
    details: non-nullable EventDetails object
    recurrence: nullable Recurrence object
    inviteList: non-nullable InviteList object
}
```
```
type Guest {
    username: non-nullable unique id of user
    rsvp: non-nullable enum [PENDING|DECLINED|UNLIKELY|MAYBE|LIKELY|ACCEPTED]
}
```
```
type GuestGroup {
    id: non-nullable unique ID of group
    name: non-nullable display name of group
    guests: non-nullable list of Guest objects with one object for each member of the group
}
```
```
type GuestList {
    groups: non-nullable list of GuestGroup objects
    guests: non-nullable list of Guest objects
} 
```
```
type Comment {
    id: immutable and unique within event occurrence (nullable/ignored on POST, required otherwise)
    username: username of comment poster
    time: ISO-8601 datetime when comment was posted
    message: non-nullable contents of comment
}
```
```
type EventOccurrence {
    id: non-nullable immutable and unique within event series
    details: non-nullable EventDetails object
    guestList: non-nullable GuestList object
    comments: non-nullable list of Comment objects
}
```
```
type Event {
    id: non-nullable unique and immutable ID of event
    details: non-nullable EventDetails object
    recurrence: nullable Recurrence object
    guestList: non-nullable GuestList object
    nextOccurrence: nullable EventSummary object (provided if there is a next occurrence)
    previousOccurrence: nullable EventSummary object (provided if there was a previous occurrence)
}
```